#include "query_funcs.h"

#include <iomanip>

void add_player(connection *C, int team_id, int jersey_num, string first_name,
                string last_name, int mpg, int ppg, int rpg, int apg,
                double spg, double bpg) {
  try {
    work W(*C);
    string head =
        "INSERT INTO PLAYER (TEAM_ID, UNIFORM_NUM, FIRST_NAME, "
        "LAST_NAME, MPG, PPG,"
        "RPG, APG, SPG, BPG) ";
    string valueLine =
        "VALUES (" + to_string(team_id) + ", " + to_string(jersey_num) + ", " +
        W.quote(first_name) + ", " + W.quote(last_name) + ", " +
        to_string(mpg) + ", " + to_string(ppg) + ", " + to_string(rpg) + ", " +
        to_string(apg) + ", " + to_string(spg) + ", " + to_string(bpg) + ");";
    string sql = head + valueLine;
    W.exec(sql);
    W.commit();
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}

void add_team(connection *C, string name, int state_id, int color_id, int wins,
              int losses) {
  try {
    work W(*C);
    string head = "INSERT INTO TEAM (NAME, STATE_ID, COLOR_ID, WINS, LOSSES) ";
    string valueLine = "VALUES (" + W.quote(name) + ", " + to_string(state_id) +
                       ", " + to_string(color_id) + ", " + to_string(wins) +
                       ", " + to_string(losses) + ");";
    string sql = head + valueLine;
    W.exec(sql);
    W.commit();
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}

void add_state(connection *C, string name) {
  string head = "INSERT INTO STATE (NAME) ";
  try {
    work W(*C);
    string valueLine = "VALUES (" + W.quote(name) + ");";
    string sql = head + valueLine;
    W.exec(sql);
    W.commit();
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}

void add_color(connection *C, string name) {
  string head = "INSERT INTO COLOR (NAME) ";
  try {
    work W(*C);
    string valueLine = "VALUES (" + W.quote(name) + ");";
    string sql = head + valueLine;
    W.exec(sql);
    W.commit();
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}

void query1(connection *C, int use_mpg, int min_mpg, int max_mpg, int use_ppg,
            int min_ppg, int max_ppg, int use_rpg, int min_rpg, int max_rpg,
            int use_apg, int min_apg, int max_apg, int use_spg, double min_spg,
            double max_spg, int use_bpg, double min_bpg, double max_bpg) {
  string sql = "SELECT * FROM PLAYER";
  string constraint;
  vector<string> constraints;
  if (use_mpg) {
    constraint =
        " MPG BETWEEN " + to_string(min_mpg) + " AND " + to_string(max_mpg);
    constraints.push_back(constraint);
  }

  if (use_ppg) {
    constraint =
        " PPG BETWEEN " + to_string(min_ppg) + " AND " + to_string(max_ppg);
    constraints.push_back(constraint);
  }

  if (use_rpg) {
    constraint =
        " RPG BETWEEN " + to_string(min_rpg) + " AND " + to_string(max_rpg);
    constraints.push_back(constraint);
  }

  if (use_apg) {
    constraint =
        " APG BETWEEN " + to_string(min_apg) + " AND " + to_string(max_apg);
    constraints.push_back(constraint);
  }

  if (use_spg) {
    constraint =
        " SPG BETWEEN " + to_string(min_spg) + " AND " + to_string(max_spg);
    constraints.push_back(constraint);
  }

  if (use_bpg) {
    constraint =
        " BPG BETWEEN " + to_string(min_bpg) + " AND " + to_string(max_bpg);
    constraints.push_back(constraint);
  }

  if (constraints.size() > 0) {
    sql += " WHERE";
    if (constraints.size() == 1) {
      sql += constraints[0];
    } else {
      for (int i = 0; i < constraints.size() - 1; ++i) {
        sql += constraints[i];
        sql += " AND";
      }
      sql += constraints.back();
    }
  }
  sql += ";";

  try {
    nontransaction N(*C);
    result R(N.exec(sql));
    cout << "PLAYER_ID TEAM_ID UNIFORM_NUM FIRST_NAME LAST_NAME MPG PPG RPG "
            "APG SPG BPG"
         << endl;
    for (auto it = R.begin(); it != R.end(); ++it) {
      cout << it[0].as<int>() << " " << it[1].as<int>() << " "
           << it[2].as<int>() << " " << it[3].as<string>() << " "
           << it[4].as<string>() << " " << it[5].as<int>() << " "
           << it[6].as<int>() << " " << it[7].as<int>() << " "
           << it[8].as<int>() << " " << fixed << setprecision(1)
           << it[9].as<double>() << " " << it[10].as<double>() << endl;
    }
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}

void query2(connection *C, string team_color) {
  try {
    nontransaction N(*C);
    string sql =
        "SELECT T.NAME FROM TEAM AS T, COLOR AS C WHERE T.COLOR_ID = "
        "C.COLOR_ID";
    sql += (" AND C.NAME = " + N.quote(team_color));
    result R(N.exec(sql));
    cout << "NAME" << endl;
    for (auto it = R.begin(); it != R.end(); ++it) {
      cout << it[0].as<string>() << endl;
    }
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}

void query3(connection *C, string team_name) {
  try {
    nontransaction N(*C);
    string sql =
        "SELECT P.FIRST_NAME, P.LAST_NAME FROM TEAM AS T, PLAYER AS P WHERE "
        "T.TEAM_ID = P.TEAM_ID";
    sql += (" AND T.NAME = " + N.quote(team_name));
    sql += " ORDER BY P.PPG DESC;";
    result R(N.exec(sql));
    cout << "FIRST_NAME LAST_NAME" << endl;
    for (auto it = R.begin(); it != R.end(); ++it) {
      cout << it[0].as<string>() << " " << it[1].as<string>() << endl;
    }
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}

void query4(connection *C, string team_state, string team_color) {
  try {
    nontransaction N(*C);
    string sql =
        "SELECT P.UNIFORM_NUM, P.FIRST_NAME, P.LAST_NAME FROM PLAYER AS P, "
        "TEAM AS T, STATE AS S, COLOR AS C WHERE "
        "P.TEAM_ID = T.TEAM_ID AND T.STATE_ID = S.STATE_ID AND T.COLOR_ID = "
        "C.COLOR_ID";
    sql += (" AND S.NAME = " + N.quote(team_state));
    sql += (" AND C.NAME = " + N.quote(team_color));
    sql += ";";
    result R(N.exec(sql));
    cout << "UNIFORM_NUM FIRST_NAME LAST_NAME" << endl;
    for (auto it = R.begin(); it != R.end(); ++it) {
      cout << it[0].as<int>() << " " << it[1].as<string>() << " "
           << it[2].as<string>() << endl;
    }
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}

void query5(connection *C, int num_wins) {
  try {
    nontransaction N(*C);
    string sql =
        "SELECT P.FIRST_NAME, P.LAST_NAME, T.NAME, T.WINS FROM PLAYER AS P, "
        "TEAM AS T WHERE P.TEAM_ID = T.TEAM_ID";
    sql += (" AND T.WINS > " + to_string(num_wins));
    sql += ";";
    result R(N.exec(sql));
    cout << "FIRST_NAME LAST_NAME NAME WINS" << endl;
    for (auto it = R.begin(); it != R.end(); ++it) {
      cout << it[0].as<string>() << " " << it[1].as<string>() << " "
           << it[2].as<string>() << " " << it[3].as<int>() << endl;
    }
  } catch (const std::exception &e) {
    cerr << e.what() << endl;
  }
}
