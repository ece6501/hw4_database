#ifndef __DATABASEHANDLER_HPP__
#define __DATABASEHANDLER_HPP__

#include <pqxx/pqxx>
#include <vector>

using namespace std;
using namespace pqxx;

class DataBaseHandler {
 private:
  connection* C;
  vector<string> tables;
  int dropTable(const string& table);

 public:
  DataBaseHandler(connection* _C) : C(_C), tables({"PLAYER", "TEAM", "STATE", "COLOR"}){}
  int dropTables();
  void createEmptyTables();
  int createStateTable();
  int createColorTable();
  int createTeamTable();
  int createPlayerTable();
  void loadColorTable();
  void loadStateTable();
  void loadTeamTable();
  void loadPlayerTable();
};

#endif