#include "DataBaseHandler.hpp"

#include <fstream>
#include <iostream>

#include "query_funcs.h"
using namespace std;
using namespace pqxx;

int DataBaseHandler::dropTable(const string& table) {
  string toDrop = "DROP TABLE IF EXISTS " + table;
  try {
    work W(*(this->C));
    W.exec(toDrop);
    W.commit();
    // cout << "DROP " << table << " successfully" << endl;
  } catch (const std::exception& e) {
    cerr << e.what() << endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

int DataBaseHandler::dropTables() {
  for (const auto& table : this->tables) {
    if (this->dropTable(table) == EXIT_FAILURE) {
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}

int DataBaseHandler::createColorTable() {
  string sql;
  sql =
      "CREATE TABLE COLOR("
      "COLOR_ID SERIAL NOT NULL,"
      "NAME VARCHAR(256) NOT NULL,"
      "CONSTRAINT CPK PRIMARY KEY (COLOR_ID)"
      ");";
  try {
    work W(*C);
    W.exec(sql);
    W.commit();
    // cout << "COLOR TABLE created" << endl;
  } catch (const std::exception& e) {
    cerr << e.what() << endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

int DataBaseHandler::createStateTable() {
  string sql;
  sql =
      "CREATE TABLE STATE("
      "STATE_ID SERIAL NOT NULL,"
      "NAME VARCHAR(256) NOT NULL,"
      "CONSTRAINT SPK PRIMARY KEY (STATE_ID)"
      ");";
  try {
    work W(*C);
    W.exec(sql);
    W.commit();
    // cout << "STATE TABLE created" << endl;
  } catch (const std::exception& e) {
    cerr << e.what() << endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

int DataBaseHandler::createTeamTable() {
  string sql;
  sql =
      "CREATE TABLE TEAM("
      "TEAM_ID SERIAL NOT NULL,"
      "NAME VARCHAR(256) NOT NULL,"
      "STATE_ID INT NOT NULL,"
      "COLOR_ID INT NOT NULL,"
      "WINS INT NOT NULL,"
      "LOSSES INT NOT NULL,"
      "CONSTRAINT TPK PRIMARY KEY (TEAM_ID),"
      "CONSTRAINT TSFK FOREIGN KEY (STATE_ID) REFERENCES STATE(STATE_ID) ON "
      "DELETE "
      "SET NULL ON UPDATE CASCADE,"
      "CONSTRAINT TCFK FOREIGN KEY (COLOR_ID) REFERENCES COLOR(COLOR_ID) ON "
      "DELETE "
      "SET NULL ON UPDATE CASCADE"
      ");";
  try {
    work W(*C);
    W.exec(sql);
    W.commit();
    // cout << "TEAM TABLE created" << endl;
  } catch (const std::exception& e) {
    cerr << e.what() << endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

int DataBaseHandler::createPlayerTable() {
  string sql;
  sql =
      "CREATE TABLE PLAYER("
      "PLAYER_ID SERIAL NOT NULL,"
      "TEAM_ID INT NOT NULL,"
      "UNIFORM_NUM INT NOT NULL,"
      "FIRST_NAME VARCHAR(256) NOT NULL,"
      "LAST_NAME VARCHAR(256) NOT NULL,"
      "MPG INT NOT NULL,"
      "PPG INT NOT NULL,"
      "RPG INT NOT NULL,"
      "APG INT NOT NULL,"
      "SPG DOUBLE PRECISION NOT NULL,"
      "BPG DOUBLE PRECISION NOT NULL,"
      "CONSTRAINT PPK PRIMARY KEY (PLAYER_ID),"
      "CONSTRAINT PTFK FOREIGN KEY (TEAM_ID) REFERENCES TEAM(TEAM_ID) ON "
      "DELETE "
      "SET NULL ON UPDATE CASCADE"
      ");";
  try {
    work W(*C);
    W.exec(sql);
    W.commit();
    // cout << "PLAYER TABLE created" << endl;
  } catch (const std::exception& e) {
    cerr << e.what() << endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

void DataBaseHandler::createEmptyTables() {
  this->createColorTable();
  this->createStateTable();
  // In order to link foreign key
  this->createTeamTable();
  this->createPlayerTable();
}

void DataBaseHandler::loadColorTable() {
  ifstream color("./color.txt");
  string readIn;
  while (getline(color, readIn)) {
    stringstream ss(readIn);
    string colorId;
    string name;
    ss >> colorId;
    ss >> name;
    add_color(this->C, name);
  }
}

void DataBaseHandler::loadStateTable() {
  ifstream state("./state.txt");
  string readIn;
  while (getline(state, readIn)) {
    stringstream ss(readIn);
    string stateId;
    string name;
    ss >> stateId;
    ss >> name;
    add_state(this->C, name);
  }
}

void DataBaseHandler::loadTeamTable() {
  ifstream team("./team.txt");
  string readIn;
  while (getline(team, readIn)) {
    stringstream ss(readIn);
    string teamId, name;
    int stateId, colorId, wins, losses;
    ss >> teamId;
    ss >> name;
    ss >> stateId;
    ss >> colorId;
    ss >> wins;
    ss >> losses;
    add_team(this->C, name, stateId, colorId, wins, losses);
  }
}

void DataBaseHandler::loadPlayerTable() {
  ifstream player("./player.txt");
  string readIn;
  while (getline(player, readIn)) {
    stringstream ss(readIn);
    string playerId, firstName, lastName;
    int teamId, uniformNum, mpg, ppg, rpg, apg;
    double spg, bpg;
    ss >> playerId;
    ss >> teamId;
    ss >> uniformNum;
    ss >> firstName;
    ss >> lastName;
    ss >> mpg;
    ss >> ppg;
    ss >> rpg;
    ss >> apg;
    ss >> spg;
    ss >> bpg;
    add_player(this->C, teamId, uniformNum, firstName, lastName, mpg, ppg, rpg,
               apg, spg, bpg);
  }
}